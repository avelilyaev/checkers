﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;

namespace Checkers.Tools
{
    public static class MyNetworkClass
    {
        public static string MyName;

        public static string OpponentName;

        //Флаг автоматического определения клиент/сервер
        private static bool _isServer;

        //IP-адрес этой машины
        private static IPAddress _ipAddrLocal;

        //IP адрес противника
        private static IPAddress _ipAddrOpponent;

        //Создаем конечную точку
        private static IPEndPoint _ipEndPoint;

        // Клиентский сокет Tcp/Ip
        private static Socket _socket;

        // Серверный сокет Tcp/Ip
        private static Socket _sListener;

        private static Socket _handler;

        //Получаем IP-адрес данного устройства
        public static IPAddress GetIp()
        {
			return _ipAddrLocal ?? (from adapter in NetworkInterface.GetAllNetworkInterfaces() 
					from ip in adapter.GetIPProperties().UnicastAddresses 
					where adapter.Name == "Ethernet"
					select ip.Address).First(x => x.ToString().Contains("192.168."));
        }

	    public static void InititalizeNetworkElemtnts(IPAddress ipAddress)
        {
            _ipEndPoint = new IPEndPoint(ipAddress, 11000);
            _socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            _sListener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
        }

        public static void InitializeOpponentIp(IPAddress ip)
        {
            _ipAddrOpponent = ip;
        }

        public static void InitializeLocalIp(IPAddress ip)
        {
            _ipAddrLocal = ip;
        }

        public static void StopListen()
        {
            if (_isServer)
            {
                _sListener.Close();
            }
        }


        public static bool GetIsServer()
        {
            return _isServer;
        }
        //Определяем кто сервер, а кто клиент-------------------------------------------------------------------
        public static string DetermineWhoClientAndWhoServer(string name)
        {
            MyName = name;
            OpponentName = null;
            try
            {
                //мы - клиент

                InititalizeNetworkElemtnts(_ipAddrOpponent);

                _socket.Connect(_ipEndPoint);

                //Отправляем имя
                SendMessage(name);

                //Получаем ответ от сервера
                OpponentName = GetMessage();
            }
            catch (Exception)
            {
                //мы - сервер
                _isServer = true;

                InititalizeNetworkElemtnts(_ipAddrLocal);

                //получаем сообщение с клиента
                OpponentName = GetMessage();

                //Отвечаем клиенту
                SendMessage(name);
            }
            return OpponentName;
        }

        public static bool StartListen()
        {
            try
            {
                _sListener.Bind(_ipEndPoint);
                _sListener.Listen(10);

                _handler = _sListener.Accept();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //Получение сообщения
        public static string GetMessage()
        {
            //буфер для входящих сообщений
            var bytes = new byte[1024];

            //Количество символов
            int bytesRec = 0;

            if (_isServer)
            {
                StartListen();
                try
                {
                    bytesRec = _handler.Receive(bytes);
                }
                catch (Exception)
                {
	                // ignored
                }
            }
            else
            {
                try
                {
                    bytesRec = _socket.Receive(bytes);
                }
                catch (Exception)
                {
	                // ignored
                }
            }
            return Encoding.UTF8.GetString(bytes, 0, bytesRec);
        }

        //Отправка
        public static void SendMessage(String message)
        {
            var msg = Encoding.UTF8.GetBytes(message);

            if (_isServer)
            {
                try
                {
                    _handler.Send(msg);
                }
                catch (Exception)
                {
	                // ignored
                }
            }
            else
            {
                _socket.Send(msg);
            }
        }

        //Отключаемся
        public static void Disconnect()
        {
            try
            {
                if (_isServer)
                {
                    _handler.Shutdown(SocketShutdown.Both);
                    _handler.Close();
                    _sListener.Close();
                }
                else
                {
                    _socket.Shutdown(SocketShutdown.Both);
                    _socket.Close();
                }
                _isServer = false;
                _ipEndPoint = null;
            }
            catch (Exception)
            {
                _isServer = false;
            }
        }

		public static List<string> FoundedAddressesList;

		/// <summary>
		/// Метод для асинхронного опроса локальной сети на предмет наличия доступных устройств
		/// </summary>
		public static void ScannningNetworkAsync(IPAddress myIp)
		{
			FoundedAddressesList = new List<String>();
			for (int i = 1; i < 256; i++)
			{
				var ip = IPAddress.Parse("192.168." + Convert.ToString(myIp.GetAddressBytes()[2]) + "." + Convert.ToString(i));
				if (!ip.Equals(myIp))
				{
					var pingSender = new Ping();
					pingSender.PingCompleted += Program.EntryForm.pingSender_Complete;
					pingSender.SendAsync(ip.ToString(), 5, Encoding.ASCII.GetBytes("test"),
						new PingOptions(5, true));
				}
			}
		}
    }
}
