﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Checkers.Tools
{
    public static class BoardManager
    {
        //Координаты убиенной фишки
        public static string KilledShapeCoords;

        //Константы всех задействованных цветов
        public static Color WhiteCell = Color.FromArgb(0, 0, 0, 0);
        public static Color BlackCell = Color.Brown;
        public static Color WhiteShape = Color.NavajoWhite;
        public static Color BlackShape = Color.SaddleBrown;
        public static Color WhiteQueen = Color.FromArgb(185, 122, 87);
        public static Color BlackQueen = Color.FromArgb(238, 175, 130);

        public static void DrawBoard(Bitmap bmp, PictureBox pictureBox)
        {
            GraphicOperations.DrawBoard(bmp);
	        Color firstColor = MyNetworkClass.GetIsServer() ? BlackShape : WhiteShape;
            Color secondColor = MyNetworkClass.GetIsServer() ? WhiteShape : BlackShape;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 8; j += 2)
                {
                    var coordsBlack = "" + Convert.ToChar((i % 2 == 0 ? 66 : 65) + j);
                    coordsBlack += 8 - i;
                    DrawShape(coordsBlack, firstColor, bmp, pictureBox);
                    var coordsWhite = "" + Convert.ToChar((i % 2 == 1 ? 66 : 65) + j);
                    coordsWhite += 3 - i;
                    DrawShape(coordsWhite, secondColor, bmp, pictureBox);
                }
            }
        }

        /// <summary>
        /// Get symbol coordinates from 
        /// Mouse coordinates
        /// </summary>
        /// <param name="mouseCoords"></param>
        /// <returns>
        /// Coordinates, example "A1", "E4", etc
        /// </returns>
        public static string GetSymbolCoordsFromMouseCoords(Point mouseCoords)
        {
            String gameCoords = null;
            for (int i = 0; i < 2; i++)
            {
	            var coords = i == 0 ? mouseCoords.X : mouseCoords.Y;
                var borderValue = 0;
                var value = -1;
                for (int j = 0; j < 8; j++)
                {
	                if (coords >= borderValue && coords <= borderValue + 80)
                    {
                        value = j;
                        break;
                    }
	                borderValue += 81;
                }
	            if (i == 0)
                    gameCoords += Convert.ToChar(65 + value);
                else
                    gameCoords += (8 - value);
            }
            return gameCoords;
        }

        /// <summary>
        /// Get Point with integer coordinates
        /// From Symbol Game coordinates
        /// </summary>
        /// <param name="coords"></param>
        /// <returns>Point with values {0-7; 0-7}</returns>
        public static Point GetIntCoordsFromSymbolCoords(string coords)
        {
            return new Point((Convert.ToInt16(coords[0]) - 65),
                             (8 - Convert.ToInt16(coords[1]) + 48));
        }

        /// <summary>
        /// Get Symbol Game Coords From Int values {0-7; 0-7}
        /// </summary>
        /// <param name="point"></param>
        /// <returns>Game coords</returns>
        public static string GetSymbolCoordsFromInt(Point point)
        {
            string returnCoords = null;
            returnCoords += Convert.ToChar(point.X + 65);
            returnCoords += 8 - point.Y;
            return returnCoords;
        }

        public static Point GetCenterFromSymbolCoords(string coords)
        {
            return (new Point((Convert.ToInt16(coords[0]) - 65) * 81 + 40,
                (8 - Convert.ToInt16(coords[1]) + 48) * 81 + 40));
        }

        /// <summary>
        /// Drawing Shape
        /// </summary>
        /// <param name="coords"></param>
        /// <param name="clr"></param>
        /// <param name="bmp"></param>
        /// <param name="pictureBox"></param>
        public static void DrawShape(string coords, Color clr, Bitmap bmp, PictureBox pictureBox)
        {
            Point pnt = GetCenterFromSymbolCoords(coords);
            GraphicOperations.Circle(pnt.X, pnt.Y, 30, Color.Black, bmp);
            GraphicOperations.FloodFill(new Point(pnt.X, pnt.Y), clr, bmp);
            GraphicOperations.Circle(pnt.X, pnt.Y, 25, Color.Black, bmp);
            GraphicOperations.Circle(pnt.X, pnt.Y, 12, Color.Black, bmp);
            GraphicOperations.Circle(pnt.X, pnt.Y, 8, Color.Black, bmp);
            pictureBox.Invalidate();
        }

        /// <summary>
        /// Remove Shape
        /// </summary>
        /// <param name="coords"></param>
        /// <param name="bmp"></param>
        /// <param name="pictureBox"></param>
        public static void RemoveShape(string coords, Bitmap bmp, PictureBox pictureBox)
        {
            var pnt = GetCenterFromSymbolCoords(coords);
            var clr = bmp.GetPixel(pnt.X, pnt.Y);

            if (IsEqualColors(clr, BlackShape) || IsEqualColors(clr, WhiteShape))
            {
                {
                    GraphicOperations.Circle(pnt.X, pnt.Y, 30, bmp.GetPixel(pnt.X, pnt.Y), bmp);
                    GraphicOperations.Circle(pnt.X, pnt.Y, 25, bmp.GetPixel(pnt.X, pnt.Y), bmp);
                    GraphicOperations.Circle(pnt.X, pnt.Y, 12, bmp.GetPixel(pnt.X, pnt.Y), bmp);
                    GraphicOperations.Circle(pnt.X, pnt.Y, 8, bmp.GetPixel(pnt.X, pnt.Y), bmp);
                    GraphicOperations.FloodFill(new Point(pnt.X, pnt.Y), BlackCell, bmp);
                    pictureBox.Invalidate();
                }
            }
            else if (IsEqualColors(clr, WhiteQueen) || IsEqualColors(clr, BlackQueen))
            {
                Color bckgrndColor = IsEqualColors(clr, WhiteQueen) ? WhiteShape : BlackShape;
                GraphicOperations.FloodFill(new Point(pnt.X, pnt.Y), Color.Black, bmp);
                GraphicOperations.FloodFill(new Point(pnt.X, pnt.Y), bckgrndColor, bmp);
                GraphicOperations.FloodFill(new Point(pnt.X, pnt.Y), Color.Black, bmp);
                GraphicOperations.FloodFill(new Point(pnt.X, pnt.Y), bckgrndColor, bmp);
                GraphicOperations.FloodFill(new Point(pnt.X, pnt.Y), Color.Black, bmp);
                GraphicOperations.FloodFill(new Point(pnt.X, pnt.Y), BlackCell, bmp);
                pictureBox.Invalidate();
            }
        }

        public static void Line(int x, int y, int x1, int y1, Bitmap bmp)
        {
            GraphicOperations.Line(x, y, x1, y1, Color.Black, bmp);
        }

        public static void DrawShapeQueen(string coords, Color clr, Bitmap bmp, PictureBox pictureBox)
        {
            var pnt = GetCenterFromSymbolCoords(coords);
            int x = pnt.X;
            int y = pnt.Y;
            GraphicOperations.Circle(x, y, 30, Color.Black, bmp);
            GraphicOperations.FloodFill(new Point(x, y), clr, bmp);
            GraphicOperations.Circle(x, y, 25, Color.Black, bmp);
            Line(x, y + 15, x + 11, y + 15, bmp);
            Line(x, y + 15, x - 10, y + 15, bmp);
            Line(x - 10, y + 15, x - 17, y - 10, bmp);
            Line(x + 10, y + 15, x + 17, y - 10, bmp);
            Line(x - 17, y - 10, x - 5, y + 5, bmp);
            Line(x + 17, y - 10, x + 5, y + 5, bmp);
            Line(x - 6, y + 5, x, y - 12, bmp);
            Line(x + 6, y + 5, x, y - 12, bmp);

            clr = IsEqualColors(clr, WhiteShape) ? WhiteQueen : BlackQueen;
            GraphicOperations.FloodFill(new Point(x, y), clr, bmp);

            pictureBox.Invalidate();
        }

        /// <summary>
        /// Compare Colors
        /// </summary>
        /// <param name="color1"></param>
        /// <param name="color2"></param>
        /// <returns></returns>
        public static bool IsEqualColors(Color color1, Color color2)
        {
            return color1.ToArgb().Equals(color2.ToArgb());
        }

		public static void RefreshScore(bool isBlackColor, Label label1, Label label2, int countOfKillsWhite, int countOfKillsBlack)
        {
            label1.Text = MyNetworkClass.MyName + " : " + Convert.ToString(isBlackColor ? countOfKillsWhite : countOfKillsBlack);
            label2.Text = MyNetworkClass.OpponentName + " : " + Convert.ToString(isBlackColor ? countOfKillsBlack : countOfKillsWhite);
        }

        //Метод, проверяющий валидность клетки (Клетка валидна, если она не белая и свободная)
        public static bool IsValidCell(string coords, Bitmap bmp)
        {
            var pnt = GetCenterFromSymbolCoords(coords);
            var clr = bmp.GetPixel(pnt.X, pnt.Y);
			//Клетка не должна быть белой и должна быть свободной
            return !IsEqualColors(clr, WhiteCell) && IsEqualColors(clr, BlackCell);
        }

        //Получаем цвет клетки (черный, белый или null)
        //null - клетка не пригодна для начала хода
        public static string GetColorOfCell(string coords, Bitmap bmp)
        {
            var pnt = GetCenterFromSymbolCoords(coords);
            Color clr;
            try
            {
                clr = bmp.GetPixel(pnt.X, pnt.Y);
            }
            catch (Exception)
            {
                return "Error";
            }

            //возвращаем черный цвет фишки
            if (IsEqualColors(clr, BlackShape))
            {
                return "Black";
            }

            //возвращаем белый цвет фишки
            if (IsEqualColors(clr, WhiteShape))
            {
                return "White";
            }

            //возвращаем черный цвет фишки-дамки
            if (IsEqualColors(clr, BlackQueen))
            {
                return "BlackQueen";
            }

            //возвращаем белый цвет фишки-дамки
            if (IsEqualColors(clr, WhiteQueen))
            {
                return "WhiteQueen";
            }

            //возвращаем цвет пустой клетки
            if (IsEqualColors(clr, BlackCell))
            {
                return "BlackCell";
            }
            return null;
        }

        public static string GetColorOfCell(Point cell, Bitmap bmp)
        {
            return GetColorOfCell(GetSymbolCoordsFromInt(new Point(cell.X, cell.Y)), bmp);
        }

        public static void HighlightCell(string coords, bool onOff, Bitmap bmp, PictureBox pictureBox)
        {
            var color = GetColorOfCell(coords, bmp);
            if (color != null && color != "Error")
            {
                var pnt = GetCenterFromSymbolCoords(coords);
                pnt.X += 32;
                var clr = onOff ? Color.Gold : BlackCell;
                GraphicOperations.FloodFill(pnt, clr, bmp);
                pictureBox.Invalidate();
            }
        }

        private static int GetDeltaX(Point firstPnt, Point secondPnt)
        {
            return firstPnt.X - secondPnt.X;
        }

        private static int GetDeltaY(Point firstPnt, Point secondPnt)
        {
            return firstPnt.Y - secondPnt.Y;
        }

        private static Point NormalizationCoordinates(Point coords)
        {
            return new Point(coords.X * 81 + 40, coords.Y * 81 + 40);
        }

        public static string IsValidDistance(string coords, bool isBlack, string firstCoords, Bitmap bmp)
        {
            //Возвращаемое значение этого метода
	        //--------------------------------------------------------------------------------------------
            //Начальные и конечные координаты
            var firstPoint = GetIntCoordsFromSymbolCoords(firstCoords);
            var secondPoint = GetIntCoordsFromSymbolCoords(coords);
            //--------------------------------------------------------------------------------------------
            //цвет бъющей фигурки
            var firstColor = GetColorOfCell(firstCoords, bmp);
            //--------------------------------------------------------------------------------------------
            //Разница между начальным и конечным положением по X и по Y
            var dx = GetDeltaX(secondPoint, firstPoint);
            var dy = GetDeltaY(secondPoint, firstPoint);
            //--------------------------------------------------------------------------------------------
            //Простое движение простой фигурки
            var isMove = Math.Abs(dx) == 1 && dy == -1;
            var returnValue = isMove ? "move" : null;
            //--------------------------------------------------------------------------------------------
			//Взятие фишки
            if (Math.Abs(dx) == 2 && Math.Abs(dy) == 2)
            {
                firstPoint = new Point(dx > 0 ? firstPoint.X + 1 : firstPoint.X - 1, firstPoint.Y + Math.Sign(dy));
                firstPoint = NormalizationCoordinates(firstPoint);
                KilledShapeCoords = GetSymbolCoordsFromMouseCoords(firstPoint);
                var clr = bmp.GetPixel(firstPoint.X, firstPoint.Y);
                var isValidColor = isBlack ? (IsEqualColors(clr, WhiteShape) || IsEqualColors(clr, WhiteQueen)) :
                    (IsEqualColors(clr, BlackShape) || IsEqualColors(clr, BlackQueen));
                firstPoint = GetIntCoordsFromSymbolCoords(firstCoords);
                returnValue = isValidColor ? "hit" : returnValue;
            }
            //---------------------------------------------------------------------------------------------
            //Простое перемещение дамки
            var isMoveQueen = Math.Abs(dx) == Math.Abs(dy) &&
                (firstColor == "BlackQueen" || firstColor == "WhiteQueen");
            if (isMoveQueen)
            {
                var x = firstPoint.X + Math.Sign(dx);
                var y = firstPoint.Y + Math.Sign(dy);
                for (int i = 0; i < Math.Abs(dx); i++)
                {
                    if (GetColorOfCell(new Point(x, y), bmp) != "BlackCell")
                    {
                        isMoveQueen = false;
                        break;
                    }
                    x += Math.Sign(dx);
                    y += Math.Sign(dy);
                }
                returnValue = isMoveQueen ? "MoveQueen" : returnValue;
            }
            //----------------------------------------------------------------------------------------------
            //Удар дамкой
            var isHitQueen = Math.Abs(dx) == Math.Abs(dy) &&
                (firstColor == "BlackQueen" || firstColor == "WhiteQueen");
            if (isHitQueen)
            {
	            int x = firstPoint.X + Math.Sign(dx);
                int y = firstPoint.Y + Math.Sign(dy);
                var count = 0;
                for (int i = 0; i < Math.Abs(dx); i++)
                {
					//цвет сканируемой клетки
                    var currentColor = GetColorOfCell(new Point(x, y), bmp);
                    //все клетки кроме одной должны быть цветом BlackCell, а та одна - противоположного бъющей
                    if (firstColor == "WhiteQueen")
                    {
	                    if (currentColor != "BlackCell" && currentColor != "Black" && currentColor != "BlackQueen")
                        {
                            isHitQueen = false;
                            break;
                        }
	                    if (currentColor == "Black" || currentColor == "BlackQueen")
	                    {
		                    count++;
		                    KilledShapeCoords = GetSymbolCoordsFromInt(new Point(x, y));
	                    }
                    }
                    else if (firstColor == "BlackQueen")
                    {
	                    if (currentColor != "BlackCell" && currentColor != "White" && currentColor != "WhiteQueen")
                        {
                            isHitQueen = false;
                            break;
                        }
	                    if (currentColor == "White" || currentColor == "WhiteQueen")
	                    {
		                    count++;
		                    KilledShapeCoords = GetSymbolCoordsFromInt(new Point(x, y));
	                    }
                    }
	                x += Math.Sign(dx);
                    y += Math.Sign(dy);
                }

                if (count != 1)
                {
                    isHitQueen = false;
                }
                returnValue = isHitQueen ? "HitQueen" : returnValue;
            }
            return returnValue;
        }

        public static bool IsKillStreak(string coords, bool isBlack, string mode, Bitmap bmp)
        {
            var currentCoords = GetIntCoordsFromSymbolCoords(coords);
            var returnFlag = false;
            var opponentColor = isBlack ? "White" : "Black";
            if (mode == "hit")
            {
                if (GetColorOfCell(new Point(currentCoords.X + 1, currentCoords.Y - 1), bmp) == opponentColor)
                {
                    if (GetColorOfCell(new Point(currentCoords.X + 2, currentCoords.Y - 2), bmp) == "BlackCell")
                    {
                        returnFlag = true;
                    }
                }
                if (GetColorOfCell(new Point(currentCoords.X + 1, currentCoords.Y + 1), bmp) == opponentColor)
                {
                    if (GetColorOfCell(new Point(currentCoords.X + 2, currentCoords.Y + 2), bmp) == "BlackCell")
                    {
                        returnFlag = true;
                    }
                }
                if (GetColorOfCell(new Point(currentCoords.X - 1, currentCoords.Y - 1), bmp) == opponentColor)
                {
                    if (GetColorOfCell(new Point(currentCoords.X - 2, currentCoords.Y - 2), bmp) == "BlackCell")
                    {
                        returnFlag = true;
                    }
                }
                if (GetColorOfCell(new Point(currentCoords.X - 1, currentCoords.Y + 1), bmp) == opponentColor)
                {
                    if (GetColorOfCell(new Point(currentCoords.X - 2, currentCoords.Y + 2), bmp) == "BlackCell")
                    {
                        returnFlag = true;
                    }
                }
            }
            return returnFlag;
        }
    }
}
