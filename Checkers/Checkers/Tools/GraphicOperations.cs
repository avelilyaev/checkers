﻿using System;
using System.Collections;
using System.Drawing;

namespace Checkers.Tools
{
    public static class GraphicOperations
    {
        public static void DrawBoard(Bitmap image)
        {
            //Расчерчиваем линии игровой доски
            int x = 0;
            for (int i = 0; i < 9; i++)
            {
                for (int j = 0; j < image.Width; j++)
                {
                    image.SetPixel(x, j, Color.Black);
                    image.SetPixel(j, x, Color.Black);
                }
                x += 81;
            }

            //Закрашиваем клеточки
            var seedPoint = new Point(41, 41);
            for (int i = 0; i < 8; i++)
            {
                if (i % 2 == 0)
                {
                    seedPoint.X = 122;
                }
                else
                {
                    seedPoint.X = 41;
                }
                for (int j = 0; j < 4; j++)
                {
                    FloodFill(seedPoint, BoardManager.BlackCell, image);
                    seedPoint.X += 162;
                }
                seedPoint.Y += 81;
            }
        }

        public static void FloodFill(Point point, Color pixelColor2, Bitmap image)
        {
            var stack = new Stack();
            var background = image.GetPixel(point.X, point.Y);
	        stack.Push(point);
            while (stack.Count > 0)
            {
                point = (Point)stack.Pop();//Извлекаем пиксель из стека
                image.SetPixel(point.X, point.Y, pixelColor2);
                //проверяем верхнюю соседнюю точку
                point.Y--;
                var pixelColor = image.GetPixel(point.X, point.Y);
                if (point.X > 0 && point.X < image.Width - 1 && point.Y > 0 && point.Y < image.Height - 1 && (pixelColor.A != pixelColor2.A || pixelColor.B != pixelColor2.B || pixelColor.R != pixelColor2.R || pixelColor.G != pixelColor2.G))
                {
                    if (pixelColor.A == background.A && pixelColor.B == background.B && pixelColor.R == background.R && pixelColor.G == background.G)
                    {
                        image.SetPixel(point.X, point.Y, pixelColor2);
                        stack.Push(point);
                    }
                }
                point.Y++;
                //проверяем левую соседнюю точку
                point.X--;
                pixelColor = image.GetPixel(point.X, point.Y);
                if (point.X > 0 && point.X < image.Width - 1 && point.Y > 0 && point.Y < image.Height - 1 && (pixelColor.A != pixelColor2.A || pixelColor.B != pixelColor2.B || pixelColor.R != pixelColor2.R || pixelColor.G != pixelColor2.G))
                {
                    if (pixelColor.A == background.A && pixelColor.B == background.B && pixelColor.R == background.R && pixelColor.G == background.G)
                    {
                        image.SetPixel(point.X, point.Y, pixelColor2);
                        stack.Push(point);
                    }
                }
                point.X++;
                //проверяем правую соседнюю точку
                point.X++;
                pixelColor = image.GetPixel(point.X, point.Y);
                if (point.X > 0 && point.X < image.Width - 1 && point.Y > 0 && point.Y < image.Height - 1 && (pixelColor.A != pixelColor2.A || pixelColor.B != pixelColor2.B || pixelColor.R != pixelColor2.R || pixelColor.G != pixelColor2.G))
                {
                    if (pixelColor.A == background.A && pixelColor.B == background.B && pixelColor.R == background.R && pixelColor.G == background.G)
                    {
                        image.SetPixel(point.X, point.Y, pixelColor2);
                        stack.Push(point);
                    }
                }
                point.X--;
                //проверяем нижнюю соседнюю точку
                point.Y++;
                pixelColor = image.GetPixel(point.X, point.Y);
                if (point.X > 0 && point.X < image.Width - 1 && point.Y > 0 && point.Y < image.Height - 1 && (pixelColor.A != pixelColor2.A || pixelColor.B != pixelColor2.B || pixelColor.R != pixelColor2.R || pixelColor.G != pixelColor2.G))
                {
                    if (pixelColor.A == background.A && pixelColor.B == background.B && pixelColor.R == background.R && pixelColor.G == background.G)
                    {
                        image.SetPixel(point.X, point.Y, pixelColor2);
                        stack.Push(point);
                    }
                }
                point.Y--;
            }
        }

        public static void Circle(int x1, int y1, int r, Color color, Bitmap image)
        {
            var x = 0;
            var y = r;
            var delta = 1 - 2 * r;
	        while (y >= 0)
            {
                image.SetPixel(x1 + x, y1 + y, color);
                image.SetPixel(x1 + x, y1 - y, color);
                image.SetPixel(x1 - x, y1 + y, color);
                image.SetPixel(x1 - x, y1 - y, color);
                var error = 2 * (delta + y) - 1;
                if ((delta < 0) && (error <= 0))
                {
                    delta += 2 * ++x + 1;
                    continue;
                }
                error = 2 * (delta - x) - 1;
                if ((delta > 0) && (error > 0))
                {
                    delta += 1 - 2 * --y;
                    continue;
                }
                x++;
                delta += 2 * (x - y);
                y--;
            }
        }

        public static void Line(int x1, int y1, int x2, int y2, Color clr, Bitmap image)
        {
            var steep = (Math.Abs(y2 - y1) > Math.Abs(x2 - x1));
            if (steep)
            {
	            var tmp = x1;
                x1 = y1;
                y1 = tmp;
                tmp = x2;
                x2 = y2;
                y2 = tmp;
            }

            if (x1 > x2)
            {
	            var tmp = x1;
                x1 = x2;
                x2 = tmp;

                tmp = y1;
                y1 = y2;
                y2 = tmp;
            }

            float dx = Math.Abs(x2 - x1);
            float dy = Math.Abs(y2 - y1);

            var error = dx / 2.0f;
            var ystep = (y1 < y2) ? 1 : -1;
            var y = y1;

            var maxX = x2;

            for (int x = x1; x < maxX; x++)
            {
                if (steep)
                {
                    image.SetPixel(y, x, clr);
                }
                else
                {
                    image.SetPixel(x, y, clr);
                }

                error -= dy;
	            if (!(error < 0)) continue;
	            y += ystep;
	            error += dx;
            }
        }
    }
}
