﻿using System;
using System.Windows.Forms;

namespace Checkers
{
	internal static class Program
    {
        public static StartForm EntryForm;
        [STAThread]
        private static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            EntryForm = new StartForm();
            Application.Run(EntryForm);
        }
    }
}
