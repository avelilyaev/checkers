﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Windows.Forms;
using Checkers.Properties;
using Checkers.Tools;

namespace Checkers
{
	public partial class StartForm : Form
	{
		public StartForm()
		{
			InitializeComponent();
		}

		private void StartForm_Load(object sender, EventArgs e)
		{
			MyNetworkClass.MyName = Environment.UserDomainName;
			var localIp = MyNetworkClass.GetIp();
			Text = @"Ваш IP-адрес : " + localIp;
			MyNetworkClass.InitializeLocalIp(localIp);
			MyNetworkClass.InititalizeNetworkElemtnts(localIp);
			MyNetworkClass.ScannningNetworkAsync(localIp);
		}

		private void refreshButton_Click(object sender, EventArgs e)
		{
			MyNetworkClass.ScannningNetworkAsync(MyNetworkClass.GetIp());
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(MyNetworkClass.OpponentName))
			{
				timer1.Stop();
				timer2.Stop();
				_myThread = null;
				Text = @"Имя вашего противника: " + MyNetworkClass.OpponentName;
				var gm = new GameForm();
				gm.Show();
			}
		}

		private void timer2_Tick(object sender, EventArgs e)
		{
			timer2.Stop();
			timer1.Stop();
			if (string.IsNullOrEmpty(MyNetworkClass.OpponentName))
			{
				MyNetworkClass.StopListen();
				Text = @"Не удалось соединиться с противником";
			}
		}

		private static void GetOpponentName()
		{
			MyNetworkClass.OpponentName = MyNetworkClass.DetermineWhoClientAndWhoServer(MyNetworkClass.MyName);
		}

		private void StartForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			MyNetworkClass.Disconnect();
		}

		//Тред для поиска противника
		private Thread _myThread;

		public void pingSender_Complete(object sender, PingCompletedEventArgs e)
		{
			if (e.Reply.Status == IPStatus.Success)
			{
				MyNetworkClass.FoundedAddressesList.Add(e.Reply.Address.ToString());
				MyNetworkClass.FoundedAddressesList = MyNetworkClass.FoundedAddressesList.Distinct().ToList();
				var index = 0;
				dataGridView2.RowCount = MyNetworkClass.FoundedAddressesList.Count;
				foreach (var address in MyNetworkClass.FoundedAddressesList)
				{
					dataGridView2[0, index].Value = address;
					index++;
				}
			}
		}

		private void dataGridView2_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
		{
			var ip = IPAddress.Parse(dataGridView2[0, e.RowIndex].Value.ToString());
			MyNetworkClass.InitializeOpponentIp(ip);

			//Запускаем цикл ожидания противника
			timer1.Enabled = true;

			//Запускаем таймер ограничения времени, чтобы если что прерваться по тайм-ауту
			timer2.Enabled = true;

			//Если еще не запустили поток на поиск противника или уже запустили и время вышло
			if (_myThread == null || _myThread.ThreadState == ThreadState.Stopped)
			{
				//Запускаем поток на поиск противника
				_myThread = new Thread(GetOpponentName);
				_myThread.Start();

				//Выводим форму ожидания
				Text = @"Идет сопряжение с противником...";
			}
		}
	}
}
