﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Checkers.Properties;
using Checkers.Tools;

namespace Checkers
{
    public partial class GameForm : Form
    {
        //Is clicked on shape
	    private bool _isClicked;

        //Координаты выбранной фишки
	    private static string _firstCoords;

        //Сообщение для/от противника с данными о ходе
	    private static string _message;

	    private static string _mode;

        //Флаг для организации очередности ходов
	    private static bool _isMyTurn;

	    private static int _countOfKillsWhite;

	    private static int _countOfKillsBlack;

        public GameForm()
        {
            InitializeComponent();
            _bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.Image = _bmp;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GameForm_Load(object sender, EventArgs e)
        {
            _isMyTurn = MyNetworkClass.GetIsServer();
            Program.EntryForm.Hide();
            
            //Рисуем сетку и расставляем фишки
            BoardManager.DrawBoard(_bmp, pictureBox1);
            
            //Обновляем метки на форме
            RefreshLabels();
            BoardManager.RefreshScore(!MyNetworkClass.GetIsServer(), label17, label18, _countOfKillsWhite, _countOfKillsBlack);
            
            //Обновляем изображение
            pictureBox1.Invalidate();

            //Говорим, чей ход
            RefreshTitle();
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (_isMyTurn)
            {
                var coords = BoardManager.GetSymbolCoordsFromMouseCoords(new Point(e.X, e.Y));

                //первая часть хода----------------------------------------1
                if (!_isClicked)
                {
                    var colorOfCell = BoardManager.GetColorOfCell(coords, _bmp);

                    var isCorrectCondition = (colorOfCell == "Black" || colorOfCell == "BlackQueen") && !MyNetworkClass.GetIsServer()
                                           || (colorOfCell == "White" || colorOfCell == "WhiteQueen") && MyNetworkClass.GetIsServer();
                    if (isCorrectCondition)
                    //-------------------------------------
                    {
                        _isClicked = true;
                        _firstCoords = coords;
                        BoardManager.HighlightCell(_firstCoords, true, _bmp, pictureBox1);
                        _message += coords;
                    }
                }
                //вторая часть хода--------------------------------------2
                else
                {
                    //Если ткнули на эту же фишку, то отменяем ее выбор
                    if (coords == _firstCoords)
                    {
                        BoardManager.HighlightCell(_firstCoords, false, _bmp, pictureBox1);
                        _isClicked = false;
                        _message = null;
                    }
                    else
                        if (BoardManager.IsValidCell(coords, _bmp))
                        {
                            //Сервер играет белыми
                            var isBlackColor = !MyNetworkClass.GetIsServer();
                            //true - доступ к управлению черными
                            //false - доступ к управлению белыми

                            _mode = BoardManager.IsValidDistance(coords, isBlackColor, _firstCoords, _bmp);
                            if (_mode != null)
                            {
                                _message += coords;
                                _isClicked = false;
                                BoardManager.HighlightCell(_firstCoords, false, _bmp, pictureBox1);

                                BoardManager.RemoveShape(_firstCoords, _bmp, pictureBox1);

                                if (coords[1] == '8')
                                {
                                    BoardManager.DrawShapeQueen(coords, isBlackColor ? BoardManager.BlackShape : BoardManager.WhiteShape, _bmp, pictureBox1);
                                }
                                else
                                {
                                    if (_mode == "move" || _mode == "hit")
                                    {
                                        BoardManager.DrawShape(coords, isBlackColor ? BoardManager.BlackShape : BoardManager.WhiteShape, _bmp, pictureBox1);
                                    }
                                    else if (_mode == "MoveQueen" || _mode == "HitQueen")
                                    {
                                        BoardManager.DrawShapeQueen(coords, isBlackColor ? BoardManager.BlackShape : BoardManager.WhiteShape, _bmp, pictureBox1);
                                    }

                                }
                                if (_mode == "hit" || _mode == "HitQueen")
                                {
                                    BoardManager.RemoveShape(BoardManager.KilledShapeCoords, _bmp, pictureBox1);
                                    if (isBlackColor)
                                    {
                                        _countOfKillsWhite++;
                                    }
                                    else
                                    {
                                        _countOfKillsBlack++;
                                    }
                                    BoardManager.RefreshScore(isBlackColor, label17, label18, _countOfKillsWhite, _countOfKillsBlack);
                                }
                                //флаг, отвечающий за взятие фишек подряд
	                            //Проверяем, есть ли еще возможность побить
                                var killStreak = BoardManager.IsKillStreak(coords, isBlackColor, _mode, _bmp);
                                _message = killStreak ? _message + "#" : _message;
                                //Походили, отправили данные о сделаном ходе и передали полномочия другому игроку
                                MyNetworkClass.SendMessage(_message);
                                _isMyTurn = killStreak ? _isMyTurn : !_isMyTurn;
                                RefreshTitle();
                                if (killStreak)
                                {
                                    _firstCoords = coords;
                                    _isClicked = true;
                                    _message = coords;
									Text = @"Взятие фишек подряд";
                                }
                                if (!isBlackColor && _countOfKillsBlack == 12 || isBlackColor && _countOfKillsWhite == 12)
                                {
									Text = MyNetworkClass.MyName + @", поздравляем, вы победили!";
                                }
                            }
                        }
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_myThread == null)
            {
                _myThread = new Thread(GetMessege);
                _myThread.Start();
            }
            if (_message != string.Empty && _myThread.ThreadState == ThreadState.Stopped && _message != "exit" && !_isMyTurn && _message != null)
            {
                var fstCoords = ConvertCoords(_message.Substring(0, 2));
                var coords = ConvertCoords(_message.Substring(2, 2));

	            var mode = BoardManager.IsValidDistance(coords, MyNetworkClass.GetIsServer(), fstCoords, _bmp);
                if (mode == "hit" || mode == "HitQueen")
                {
                    BoardManager.RemoveShape(BoardManager.KilledShapeCoords, _bmp, pictureBox1);
                    var isBlackColor = !MyNetworkClass.GetIsServer();
                    if (isBlackColor)
                    {
                        _countOfKillsBlack++;
                    }
                    else
                    {
                        _countOfKillsWhite++;
                    }
                    BoardManager.RefreshScore(isBlackColor, label17, label18, _countOfKillsWhite, _countOfKillsBlack);
                }

                var colorOfCell = BoardManager.GetColorOfCell(fstCoords, _bmp);
                BoardManager.RemoveShape(fstCoords, _bmp, pictureBox1);
                var isQueen = colorOfCell == "WhiteQueen" || colorOfCell == "BlackQueen";

                if (coords[1] == '1')
                {
                    BoardManager.DrawShapeQueen(coords, MyNetworkClass.GetIsServer() ? BoardManager.BlackShape : BoardManager.WhiteShape, _bmp, pictureBox1);
                }
                else
                {
                    if (isQueen)
                    {
                        BoardManager.DrawShapeQueen(coords, MyNetworkClass.GetIsServer() ? BoardManager.BlackShape : BoardManager.WhiteShape, _bmp, pictureBox1);
                    }
                    else
                    {
                        BoardManager.DrawShape(coords, MyNetworkClass.GetIsServer() ? BoardManager.BlackShape : BoardManager.WhiteShape, _bmp, pictureBox1);
                    }
                }
                //если сообщение с ходом заканчивается на символ "#", то это значит, что противник берет несколько фишек подряд
                _isMyTurn = _message[_message.Length - 1] == '#' ? _isMyTurn : !_isMyTurn;
                RefreshTitle();
                if (!MyNetworkClass.GetIsServer() && _countOfKillsBlack == 12 || MyNetworkClass.GetIsServer() && _countOfKillsWhite == 12)
                {
					Text = MyNetworkClass.MyName + @", вы проиграли";
                    _isMyTurn = false;
                }
                _message = null;
                _myThread = null;
            }
            else if (_message == "exit")
            {
				Text = @"Противник решил покинуть игру!";
                _message = null;
                _isMyTurn = false;
            }
        }

        private static string ConvertCoords(string coords)
        {
            var returnCoords = string.Empty;
            returnCoords += Convert.ToChar(8 - (coords[0] - 65) + 64);
            returnCoords += Convert.ToString(8 - (Convert.ToInt32(coords[1]) - 48) + 1);
            
            return returnCoords;
        }

		private static void GetMessege()
		{
			_message = MyNetworkClass.GetMessage();
		}

        private void GameForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //не работает
			if (Text != @"Противник решил покинуть игру!")
            {
                _message = "exit";
                MyNetworkClass.SendMessage(_message);
                _message = null;
            }
            MyNetworkClass.Disconnect();
            ResetScore();
        }

        private void RefreshTitle()
        {
            Text = _isMyTurn ? "Ваш ход, " + MyNetworkClass.MyName
                : MyNetworkClass.OpponentName + " ходит, подождите...";
        }

        private void GameForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Program.EntryForm.Show();
        }

        public static void ResetScore()
        {
            _countOfKillsWhite = 0;
            _countOfKillsBlack = 0;
        }

        public void RefreshLabels()
        {
	        if (MyNetworkClass.GetIsServer()) return;
	        label1.Text = "H";
	        label2.Text = "G";
	        label3.Text = "F";
	        label4.Text = "E";
	        label5.Text = "D";
	        label6.Text = "C";
	        label7.Text = "B";
	        label8.Text = "A";
	        label9.Text = "1";
	        label10.Text = "2";
	        label12.Text = "3";
	        label11.Text = "4";
	        label16.Text = "5";
	        label15.Text = "6";
	        label14.Text = "7";
	        label13.Text = "8";
        }

		//Холст
		private readonly Bitmap _bmp;

		private Thread _myThread;
	}
}