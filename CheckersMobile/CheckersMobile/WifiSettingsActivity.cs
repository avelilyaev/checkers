﻿using System;
using System.Linq;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.Threading;
using System.Timers;
using System.Net;
using System.Net.NetworkInformation;
using Android.Accounts;
using Android.Net.Wifi;
using CheckersMobile.Tools;

namespace CheckersMobile
{
	[Activity(Label = "WifiSettingsActivity")]
	public class WifiSettingsActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.WifiSettings);
			_instance = this;

			_textMessage = FindViewById<TextView>(Resource.Id.textIpOrMessage);
			_ipAddressesList = FindViewById<ListView>(Resource.Id.listIp);
			var backButton = FindViewById<Button>(Resource.Id.backButton);
			backButton.Click += backButton_Click;

			_timer1 = new System.Timers.Timer(100) { Enabled = false };
			_timer1.Elapsed += timer1_Tick;
			_timer2 = new System.Timers.Timer(30000) { Enabled = false };
			_timer2.Elapsed += timer2_Tick;

			var ip = MyNetworkClass.GetIp();

			if (ip != null)
			{
				_textMessage.Text = "Ваш IP-адрес: " + ip;
				MyNetworkClass.InitializeLocalIp(ip);
				MyNetworkClass.ScannningNetworkAsync(ip);
			}
			else
			{
				new AlertDialog.Builder(this)
				.SetTitle("Важное сообщение!")
				.SetMessage("Пожалуйста, включите Wi-fi на вашем устройстве!")
				.SetPositiveButton("ОК", (senderAlert, args) =>
				{
					var wifi = (WifiManager)GetSystemService(WifiService);
					wifi.SetWifiEnabled(true); 
					Finish();
				})
				.SetNegativeButton("Выход", (senderAlert, args) => { System.Environment.Exit(0); })
				.SetIcon(Resource.Drawable.Icon).Show();
			}
			_ipAddressesList.ItemClick += ipAddressesList_ItemClick;
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			if (!string.IsNullOrEmpty(MyNetworkClass.OpponentName))
			{
				_timer1.Stop();
				_timer2.Stop();
				StartActivity(typeof(GameActivity));
				_myThread = null;
			}
		}

		public static void pingSender_Complete(object sender, PingCompletedEventArgs e)
		{
			if (e.Reply.Status == IPStatus.Success)
			{
				MyNetworkClass.FoundedAddressesList.Add(e.Reply.Address.ToString());
				_ipAddressesList.Adapter = new ArrayAdapter<string>(_instance, Android.Resource.Layout.SimpleListItem1, MyNetworkClass.FoundedAddressesList);
			}
		}

		private void timer2_Tick(object sender, EventArgs e)
		{
			_timer2.Stop();
			_timer1.Stop();
			if (string.IsNullOrEmpty(MyNetworkClass.OpponentName))
			{
				MyNetworkClass.StopListen();
				RunOnUiThread(() =>
				{
					new AlertDialog.Builder(this)
					.SetTitle("Важное сообщение!")
					.SetMessage("Не удалось соединиться с противником!")
					.SetPositiveButton("Попробовать еще", (senderAlert, args) => { Finish(); })
					.SetNegativeButton("Вот блин!", (senderAlert, args) => { System.Environment.Exit(0); })
					.SetIcon(Resource.Drawable.Icon).Show();
				});
			}
		}

		/// <summary>
		/// Обработчик нажатия на элемент списка IP адресов
		/// </summary>
		private void ipAddressesList_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			MyNetworkClass.InitializeOpponentIp(IPAddress.Parse(MyNetworkClass.FoundedAddressesList[e.Position]));
			var accountManager = AccountManager.Get(this);

			//Записываем имя игрока
			var account = accountManager.GetAccountsByType("com.google").FirstOrDefault();
			MyNetworkClass.MyName = account != null ? account.Name.Split("@".ToCharArray(0, 1), 2)[0] : Build.Model;

			//Запускаем цикл ожидания противника
			_timer1.Start();

			//Запускаем таймер для ожидания подключения
			_timer2.Start();

			//Если еще не запустили поток на поиск противника или уже запустили и время вышло
			if (_myThread == null || _myThread.ThreadState == ThreadState.Stopped)
			{
				//Запускаем поток на поиск противника
				_myThread = new Thread(GetOpponentName);
				_myThread.Start();

				//todo: Вывести анимаю с ожиданиеми и избавиться от строчки снизу
				_textMessage.Text = "Идет сопряжение с противником...";
			}
		}

		/// <summary>
		/// Обработчик нажатия на кнопку "Назад"
		/// </summary>
		private void backButton_Click(object sender, EventArgs e)
		{
			Finish();
		}

		/// <summary>
		/// Потоковая функция. Стартует автодоговор Клиент/Сервер
		/// </summary>
		private static void GetOpponentName()
		{
			MyNetworkClass.OpponentName = MyNetworkClass.DetermineWhoClientAndWhoServer(MyNetworkClass.MyName);
		}

		/// <summary>
		/// Thread for find opponent
		/// </summary>
		private Thread _myThread;

		/// <summary>
		/// This list is contain IP-address of all local network
		/// </summary>
		private static ListView _ipAddressesList;

		/// <summary>
		/// Сообщение сверху. Может содержать:
		/// Текущий IP-адрес,
		/// Предупреждение об отсутствии подключения
		/// </summary>
		private TextView _textMessage;

		/// <summary>
		/// Объект этого активити
		/// </summary>
		private static WifiSettingsActivity _instance;

		/// <summary>
		/// Этот таймер каждые 100 миллисекунд смотрит, не нашелся ли оппонент
		/// </summary>
		private System.Timers.Timer _timer1;

		/// <summary>
		/// А этот таймер запускает процесс поиска оппонента
		/// </summary>
		private System.Timers.Timer _timer2;
	}
}