using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Android.Bluetooth;
using Android.Widget;
using Java.Lang;
using Java.Util;
using Exception = System.Exception;
using String = System.String;
using Thread = System.Threading.Thread;

namespace CheckersMobile.Tools
{
	public static class MyNetworkClass
	{
		/// <summary>
		/// ��� �������� ������
		/// </summary>
		public static string MyName;

		/// <summary>
		/// ��� ����������
		/// </summary>
		public static string OpponentName;

		/// <summary>
		/// ������ ��������� Bluetooth - ���������
		/// </summary>
		public static List<BluetoothDevice> FoundDevices;

		/// <summary>
		/// MAC-����� ���������� Bluetooth - ����������
		/// </summary>
		public static string MacAddress;

		/// <summary>
		///IP-����� ���� ������
		/// </summary>
		private static IPAddress _ipAddrLocal;

		/// <summary>
		/// IP ����� ����������
		/// </summary>
		private static IPAddress _ipAddrOpponent;

		/// <summary>
		/// ������� �������� �����
		/// </summary>
		private static IPEndPoint _ipEndPoint;

		/// <summary>
		/// ���������� ����� Tcp/Ip
		/// </summary>
		private static Socket _socket;

		/// <summary>
		/// ��������� ����� Tcp/Ip
		/// </summary>
		private static Socket _sListener;

		private static Socket _handler;

		public static List<string> FoundedAddressesList;

		/// <summary>
		/// ���������� ������������� ��� ���������� ��������� ����� Bluetooth
		/// </summary>
		private static readonly UUID Uid = UUID.FromString("550e8400-e29b-41d4-a716-446655440000");

		/// <summary>
		/// Bluetooth-�����
		/// </summary>
		private static BluetoothSocket _bSocket;

		/// <summary>
		/// ����, ������������ ��� ������, � ��� ������
		/// </summary>
		private static bool _isServer;

		/// <summary>
		/// ����� ��� ������������ ������ ��������� ���� �� ������� ������� ��������� ���������
		/// </summary>
		public static void ScannningNetworkAsync(IPAddress myIp)
		{
			FoundedAddressesList = new List<String>();
			for (int i = 1; i < 256; i++)
			{
				var ip = IPAddress.Parse("192.168." + Convert.ToString(myIp.GetAddressBytes()[2]) + "." + Convert.ToString(i));
				if (!ip.Equals(myIp))
				{
					var pingSender = new Ping();
					pingSender.PingCompleted += WifiSettingsActivity.pingSender_Complete;
					pingSender.SendAsync(ip.ToString(), 5, Encoding.ASCII.GetBytes("test"),
						new PingOptions(5, true));
				}
			}
		}

		//�������� IP-����� ������� ����������
		public static IPAddress GetIp()
		{
			return _ipAddrLocal ?? (from adapter in NetworkInterface.GetAllNetworkInterfaces()
									from ip in adapter.GetIPProperties().UnicastAddresses
									where adapter.Name == "wlan0"
									select ip.Address).First(x => x.ToString().Contains("192.168."));
		}

		public static void InititalizeNetworkElemtnts(IPAddress ipAddress)
		{
			_ipEndPoint = new IPEndPoint(ipAddress, 11000);
			_socket = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
			_sListener = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
		}

		public static void InitializeOpponentIp(IPAddress ip)
		{
			_ipAddrOpponent = ip;
		}

		public static void InitializeLocalIp(IPAddress ip)
		{
			_ipAddrLocal = ip;
		}

		public static void StopListen()
		{
			if (_isServer)
			{
				_sListener.Close();
			}
		}

		public static void InitializeForSlave()
		{
			if (_bSocket == null)
			{
				BluetoothDevice device = BluetoothAdapter.DefaultAdapter.GetRemoteDevice(MacAddress);
				_bSocket = device.CreateRfcommSocketToServiceRecord(Uid);
				//������� � ��������� �����
				_bSocket.Connect();
				_isServer = false;
				MyName = BluetoothAdapter.DefaultAdapter.Name;
				SendMessage(MyName);
			}
		}

		public static void InitializeForMaster()
		{
			if (_bSocket == null)
			{
				BluetoothServerSocket serverSocket = BluetoothAdapter.DefaultAdapter.ListenUsingRfcommWithServiceRecord("Checkers", Uid);
				//������� � ��������� �����
				_bSocket = serverSocket.Accept();
				_isServer = true;
				MyName = BluetoothAdapter.DefaultAdapter.Name;
				OpponentName = GetMessage();
			}
		}

		public static bool GetIsServer()
		{
			return _isServer;
		}

		/// <summary>
		/// ���������� ��� ������, � ��� ������
		/// </summary>
		public static string DetermineWhoClientAndWhoServer(string name)
		{
			MyName = name;
			OpponentName = null;
			try
			{
				//�� - ������

				InititalizeNetworkElemtnts(_ipAddrOpponent);

				_socket.Connect(_ipEndPoint);

				//���������� ���
				SendMessage(name);

				//�������� ����� �� �������
				OpponentName = GetMessage();
			}
			catch (Exception)
			{
				//�� - ������
				_isServer = true;

				InititalizeNetworkElemtnts(_ipAddrLocal);

				//�������� ��������� � �������
				OpponentName = GetMessage();

				//�������� �������
				SendMessage(name);
			}
			return OpponentName;
		}

		public static bool StartListen()
		{
			try
			{
				_sListener.Bind(_ipEndPoint);
				_sListener.Listen(10);

				_handler = _sListener.Accept();
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		/// <summary>
		/// ��������� ���������
		/// </summary>
		public static string GetMessage()
		{
			if (_bSocket != null)
			{
				byte[] buffer = new byte[1024];
				_bSocket.InputStream.Read(buffer, 0, 1024);
				return Encoding.UTF8.GetString(buffer);
			}
			else
			{
				//����� ��� �������� ���������
				byte[] bytes = new byte[1024];

				//���������� ��������
				int bytesRec = 0;

				if (_isServer)
				{
					StartListen();
					try
					{
						bytesRec = _handler.Receive(bytes);
					}
					catch (Exception)
					{
						// ignored
					}
				}
				else
				{
					try
					{
						bytesRec = _socket.Receive(bytes);
					}
					catch (Exception)
					{
						// ignored
					}
				}
				return Encoding.UTF8.GetString(bytes, 0, bytesRec);
			}
		}

		/// <summary>
		/// �������� ���������
		/// </summary>
		public static void SendMessage(string message)
		{
			byte[] msg = Encoding.UTF8.GetBytes(message);

			if (_bSocket != null)
			{
				_bSocket.OutputStream.Write(msg, 0, message.Length);
			}
			else
			{
				if (_isServer)
				{
					try
					{
						_handler.Send(msg);
					}
					catch (Exception)
					{
						// ignored
					}
				}
				else
				{
					_socket.Send(msg);
				}
			}
		}

		/// <summary>
		/// �����������
		/// </summary>
		public static void Disconnect()
		{
			try
			{
				if (_isServer)
				{
					_handler.Shutdown(SocketShutdown.Both);
					_handler.Close();
					_sListener.Close();
				}
				else
				{
					_socket.Shutdown(SocketShutdown.Both);
					_socket.Close();
				}
				_isServer = false;
				_ipEndPoint = null;
			}
			catch (Exception)
			{
				_isServer = false;
			}
		}
	}
}