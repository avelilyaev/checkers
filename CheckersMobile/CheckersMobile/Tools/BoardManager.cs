using System;
using Android.Graphics;
using Android.Widget;

namespace CheckersMobile.Tools
{
	public static class BoardManager
	{
		//���������� �������� �����
		public static string KilledShapeCoords;

		//��������� ���� ��������������� ������
		public static Color WhiteCell = Color.Argb(0, 0, 0, 0);
		public static Color BlackCell = Color.Brown;
		public static Color WhiteShape = Color.NavajoWhite;
		public static Color BlackShape = Color.SaddleBrown;
		public static Color WhiteQueen = Color.Rgb(185, 122, 87);
		public static Color BlackQueen = Color.Rgb(238, 175, 130);


		public static void DrawBoard(Bitmap bmp, ImageView pictureBox)
		{
			GraphicOperations.DrawBoard(bmp);
			var firstColor = MyNetworkClass.GetIsServer() ? BlackShape : WhiteShape;
			var secondColor = MyNetworkClass.GetIsServer() ? WhiteShape : BlackShape;
			for (int i = 0; i < 3; i++)
			{
				for (int j = 0; j < 8; j += 2)
				{
					var coordsBlack = "" + Convert.ToChar((i % 2 == 0 ? 66 : 65) + j);
					coordsBlack += 8 - i;
					DrawShape(coordsBlack, firstColor, bmp, pictureBox);
					var coordsWhite = "" + Convert.ToChar((i % 2 == 1 ? 66 : 65) + j);
					coordsWhite += 3 - i;
					DrawShape(coordsWhite, secondColor, bmp, pictureBox);
				}
			}
		}

		/// <summary>
		/// Get symbol coordinates from 
		/// Mouse coordinates
		/// </summary>
		/// <param name="mouseCoords"></param>
		/// <returns>
		/// Coordinates, example "A1", "E4", etc
		/// </returns>
		public static string GetSymbolCoordsFromMouseCoords(Point mouseCoords)
		{
			string gameCoords = null;
			for (int i = 0; i < 2; i++)
			{
				var coords = i == 0 ? mouseCoords.X : mouseCoords.Y;
				var borderValue = 0;
				var value = -1;
				for (int j = 0; j < 8; j++)
				{
					if (coords >= borderValue && coords <= borderValue + GameActivity.ScreenWidth / 8 - 2)
					{
						value = j;
						break;
					}
					borderValue += GameActivity.ScreenWidth / 8 - 1;
				}
				if (i == 0)
					gameCoords += Convert.ToChar(65 + value);
				else
					gameCoords += (8 - value);
			}
			return gameCoords;
		}

		/// <summary>
		/// Get Point with integer coordinates
		/// From Symbol Game coordinates
		/// </summary>
		/// <param name="coords"></param>
		/// <returns>Point with values {0-7; 0-7}</returns>
		public static Point GetIntCoordsFromSymbolCoords(string coords)
		{
			return new Point((Convert.ToInt16(coords[0]) - 65),
							 (8 - Convert.ToInt16(coords[1]) + 48));
		}

		/// <summary>
		/// Get Symbol Game Coords From Int values {0-7; 0-7}
		/// </summary>
		/// <param name="point"></param>
		/// <returns>Game coords</returns>
		public static string GetSymbolCoordsFromInt(Point point)
		{
			string returnCoords = null;
			returnCoords += Convert.ToChar(point.X + 65);
			returnCoords += 8 - point.Y;
			return returnCoords;
		}

		public static Point GetCenterFromSymbolCoords(string coords)
		{
			return new Point((Convert.ToInt16(coords[0]) - 65) * (GameActivity.ScreenWidth / 8 - 1) + GameActivity.ScreenWidth / 16,
				(8 - Convert.ToInt16(coords[1]) + 48) * (GameActivity.ScreenWidth / 8 - 1) + GameActivity.ScreenWidth / 16);
		}

		/// <summary>
		/// Drawing Shape
		/// </summary>
		/// <param name="coords"></param>
		/// <param name="clr"></param>
		/// <param name="bmp"></param>
		/// <param name="pictureBox"></param>
		public static void DrawShape(string coords, Color clr, Bitmap bmp, ImageView pictureBox)
		{
			var pnt = GetCenterFromSymbolCoords(coords);
			GraphicOperations.Circle(pnt.X, pnt.Y, (GameActivity.ScreenWidth - 5) / 21, Color.Black, bmp);
			GraphicOperations.FloodFill(bmp, new Point(pnt.X, pnt.Y), clr);
			GraphicOperations.Circle(pnt.X, pnt.Y, (GameActivity.ScreenWidth - 8) / 26, Color.Black, bmp);
			GraphicOperations.Circle(pnt.X, pnt.Y, (GameActivity.ScreenWidth - 8) / 52, Color.Black, bmp);
			GraphicOperations.Circle(pnt.X, pnt.Y, (GameActivity.ScreenWidth - 8) / 104, Color.Black, bmp);
			pictureBox.Invalidate();
		}

		/// <summary>
		/// Remove Shape
		/// </summary>
		/// <param name="coords"></param>
		/// <param name="bmp"></param>
		/// <param name="pictureBox"></param>
		public static void RemoveShape(string coords, Bitmap bmp, ImageView pictureBox)
		{
			var pnt = GetCenterFromSymbolCoords(coords);
			var clr = GraphicOperations.ConvertIntToRgb(bmp.GetPixel(pnt.X, pnt.Y));

			if (IsEqualColors(clr, BlackShape) || IsEqualColors(clr, WhiteShape))
			{
				{
					GraphicOperations.Circle(pnt.X, pnt.Y, (GameActivity.ScreenWidth - 5) / 21, GraphicOperations.ConvertIntToRgb(bmp.GetPixel(pnt.X, pnt.Y)), bmp);
					GraphicOperations.Circle(pnt.X, pnt.Y, (GameActivity.ScreenWidth - 8) / 26, GraphicOperations.ConvertIntToRgb(bmp.GetPixel(pnt.X, pnt.Y)), bmp);
					GraphicOperations.Circle(pnt.X, pnt.Y, (GameActivity.ScreenWidth - 8) / 52, GraphicOperations.ConvertIntToRgb(bmp.GetPixel(pnt.X, pnt.Y)), bmp);
					GraphicOperations.Circle(pnt.X, pnt.Y, (GameActivity.ScreenWidth - 8) / 104, GraphicOperations.ConvertIntToRgb(bmp.GetPixel(pnt.X, pnt.Y)), bmp);
					GraphicOperations.FloodFill(bmp, new Point(pnt.X, pnt.Y), BlackCell);
					pictureBox.Invalidate();
				}
			}
			else if (IsEqualColors(clr, WhiteQueen) || IsEqualColors(clr, BlackQueen))
			{
				var bckgrndColor = IsEqualColors(clr, WhiteQueen) ? WhiteShape : BlackShape;
				GraphicOperations.FloodFill(bmp, new Point(pnt.X, pnt.Y), Color.Black);
				GraphicOperations.FloodFill(bmp, new Point(pnt.X, pnt.Y), bckgrndColor);
				GraphicOperations.FloodFill(bmp, new Point(pnt.X, pnt.Y), Color.Black);
				GraphicOperations.FloodFill(bmp, new Point(pnt.X, pnt.Y), bckgrndColor);
				GraphicOperations.FloodFill(bmp, new Point(pnt.X, pnt.Y), Color.Black);
				GraphicOperations.FloodFill(bmp, new Point(pnt.X, pnt.Y), BlackCell);
				pictureBox.Invalidate();
			}
		}

		public static void Line(int x, int y, int x1, int y1, Bitmap bmp)
		{
			GraphicOperations.Line(x, y, x1, y1, Color.Black, bmp);
		}

		public static void DrawShapeQueen(string coords, Color clr, Bitmap bmp, ImageView pictureBox)
		{
			var pnt = GetCenterFromSymbolCoords(coords);
			int x = pnt.X;
			int y = pnt.Y;
			var w = GameActivity.ScreenWidth;
			GraphicOperations.Circle(x, y, (w - 5) / 21, Color.Black, bmp);
			GraphicOperations.FloodFill(bmp, new Point(x, y), clr);
			GraphicOperations.Circle(x, y, (w - 8) / 26, Color.Black, bmp);
			//��������� ������
			Line(x + w / 64, y + w / 40 - 1, x - w / 64, y + w / 40 - 1, bmp);
			//����� ����� ������ ����� ������
			Line(x - w / 64, y + w / 40 - 1, x - w / 40, y - w / 64, bmp);
			//������ ����� ������ ������
			Line(x + w / 64, y + w / 40 - 1, x + w / 40, y - w / 64, bmp);
			//������ ����� ������ ����� ������
			Line(x - w / 40, y - w / 64, x - (w / 80 - 1), y + w / 160, bmp);
			//����� ����� ������� ����� ������
			Line(x + w / 40, y - w / 64, x + w / 80 - 1, y + w / 160, bmp);
			//����� ����� �������� ����� ������(����� �����)
			Line(x - (w / 80 - 1), y + w / 160 - 1, x, y - (w / 64 + 1), bmp);
			//������ ����� �������� ����� ������
			Line(x + w / 80 - 1, y + w / 160, x, y - (w / 64 + 1), bmp);
			clr = IsEqualColors(clr, WhiteShape) ? WhiteQueen : BlackQueen;
			GraphicOperations.FloodFill(bmp, new Point(x, y), clr);
			pictureBox.Invalidate();
		}

		/// <summary>
		/// Compare Colors
		/// </summary>
		/// <param name="color1"></param>
		/// <param name="color2"></param>
		/// <returns></returns>
		public static bool IsEqualColors(Color color1, Color color2)
		{
			return color1.ToArgb().Equals(color2.ToArgb());
		}

		//�����, ����������� ���������� ������ (������ �������, ���� ��� �� ����� � ���������)
		public static bool IsValidCell(string coords, Bitmap bmp)
		{
			var pnt = GetCenterFromSymbolCoords(coords);
			var clr = GraphicOperations.ConvertIntToRgb(bmp.GetPixel(pnt.X, pnt.Y));
			//������ �� ������ ���� ����� � ������ ���� ���������
			return !IsEqualColors(clr, WhiteCell) && IsEqualColors(clr, BlackCell);
		}

		//�������� ���� ������ (������, ����� ��� null)
		//null - ������ �� �������� ��� ������ ����
		public static string GetColorOfCell(string coords, Bitmap bmp)
		{
			var pnt = GetCenterFromSymbolCoords(coords);
			Color clr;
			try
			{
				clr = GraphicOperations.ConvertIntToRgb(bmp.GetPixel(pnt.X, pnt.Y));
			}
			catch (Exception)
			{
				return "Error";
			}

			//���������� ������ ���� �����
			if (IsEqualColors(clr, BlackShape))
			{
				return "Black";
			}

			//���������� ����� ���� �����
			if (IsEqualColors(clr, WhiteShape))
			{
				return "White";
			}

			//���������� ������ ���� �����-�����
			if (IsEqualColors(clr, BlackQueen))
			{
				return "BlackQueen";
			}

			//���������� ����� ���� �����-�����
			if (IsEqualColors(clr, WhiteQueen))
			{
				return "WhiteQueen";
			}

			//���������� ���� ������ ������
			if (IsEqualColors(clr, BlackCell))
			{
				return "BlackCell";
			}
			return null;
		}

		public static string GetColorOfCell(Point cell, Bitmap bmp)
		{
			return GetColorOfCell(GetSymbolCoordsFromInt(new Point(cell.X, cell.Y)), bmp);
		}

		public static void HighlightCell(string coords, bool onOff, Bitmap bmp, ImageView pictureBox)
		{
			var color = GetColorOfCell(coords, bmp);
			if (color != null && color != "Error")
			{
				var pnt = GetCenterFromSymbolCoords(coords);
				pnt.X += GameActivity.ScreenWidth/20 + 1;
				var clr = onOff ? Color.Gold : BlackCell;
				GraphicOperations.FloodFill(bmp, pnt, clr);
				pictureBox.Invalidate();
			}
		}

		private static int GetDeltaX(Point firstPnt, Point secondPnt)
		{
			return firstPnt.X - secondPnt.X;
		}

		private static int GetDeltaY(Point firstPnt, Point secondPnt)
		{
			return firstPnt.Y - secondPnt.Y;
		}

		private static Point NormalizationCoordinates(Point coords)
		{
			return new Point(coords.X * (GameActivity.ScreenWidth / 8 - 1) + GameActivity.ScreenWidth / 16, coords.Y * (GameActivity.ScreenWidth / 8 - 1) + GameActivity.ScreenWidth / 16);
		}

		public static string IsValidDistance(string coords, bool isBlack, string firstCoords, Bitmap bmp)
		{
			//��������� � �������� ����������
			var firstPoint = GetIntCoordsFromSymbolCoords(firstCoords);
			var secondPoint = GetIntCoordsFromSymbolCoords(coords);
			//--------------------------------------------------------------------------------------------
			//���� ������ �������
			var firstColor = GetColorOfCell(firstCoords, bmp);
			//--------------------------------------------------------------------------------------------
			//������� ����� ��������� � �������� ���������� �� X � �� Y
			var dx = GetDeltaX(secondPoint, firstPoint);
			var dy = GetDeltaY(secondPoint, firstPoint);
			//--------------------------------------------------------------------------------------------
			//������� �������� ������� �������
			var isMove = Math.Abs(dx) == 1 && dy == -1;
			var returnValue = isMove ? "move" : null;
			//--------------------------------------------------------------------------------------------
			//������ �������
			if (Math.Abs(dx) == 2 && Math.Abs(dy) == 2)
			{
				firstPoint = new Point(dx > 0 ? firstPoint.X + 1 : firstPoint.X - 1, firstPoint.Y + Math.Sign(dy));
				firstPoint = NormalizationCoordinates(firstPoint);
				KilledShapeCoords = GetSymbolCoordsFromMouseCoords(firstPoint);
				var clr = GraphicOperations.ConvertIntToRgb(bmp.GetPixel(firstPoint.X, firstPoint.Y));
				var isValidColor = isBlack ? IsEqualColors(clr, WhiteShape) || IsEqualColors(clr, WhiteQueen) :
					IsEqualColors(clr, BlackShape) || IsEqualColors(clr, BlackQueen);
				firstPoint = GetIntCoordsFromSymbolCoords(firstCoords);
				returnValue = isValidColor ? "hit" : returnValue;
			}
			//---------------------------------------------------------------------------------------------
			//������� ����������� �����
			var isMoveQueen = Math.Abs(dx) == Math.Abs(dy) &&
				(firstColor == "BlackQueen" || firstColor == "WhiteQueen");
			if (isMoveQueen)
			{
				int x = firstPoint.X + Math.Sign(dx);
				int y = firstPoint.Y + Math.Sign(dy);
				for (int i = 0; i < Math.Abs(dx); i++)
				{
					if (GetColorOfCell(new Point(x, y), bmp) != "BlackCell")
					{
						isMoveQueen = false;
						break;
					}
					x += Math.Sign(dx);
					y += Math.Sign(dy);
				}
				returnValue = isMoveQueen ? "MoveQueen" : returnValue;
			}
			//----------------------------------------------------------------------------------------------
			//���� ������
			var isHitQueen = Math.Abs(dx) == Math.Abs(dy) &&
				(firstColor == "BlackQueen" || firstColor == "WhiteQueen");
			if (isHitQueen)
			{
				//���� ����������� ������

				int x = firstPoint.X + Math.Sign(dx);
				int y = firstPoint.Y + Math.Sign(dy);
				var count = 0;
				for (int i = 0; i < Math.Abs(dx); i++)
				{
					var currentColor = GetColorOfCell(new Point(x, y), bmp);
					//��� ������ ����� ����� ������ ���� ������ BlackCell, � �� ���� - ���������������� ������
					if (firstColor == "WhiteQueen")
					{
						if (currentColor != "BlackCell" && currentColor != "Black" && currentColor != "BlackQueen")
						{
							isHitQueen = false;
							break;
						}
						if (currentColor == "Black" || currentColor == "BlackQueen")
						{
							count++;
							KilledShapeCoords = GetSymbolCoordsFromInt(new Point(x, y));
						}
					}
					else if (firstColor == "BlackQueen")
					{
						if (currentColor != "BlackCell" && currentColor != "White" && currentColor != "WhiteQueen")
						{
							isHitQueen = false;
							break;
						}
						if (currentColor == "White" || currentColor == "WhiteQueen")
						{
							count++;
							KilledShapeCoords = GetSymbolCoordsFromInt(new Point(x, y));
						}
					}
					x += Math.Sign(dx);
					y += Math.Sign(dy);
				}

				if (count != 1)
				{
					isHitQueen = false;
				}
				returnValue = isHitQueen ? "HitQueen" : returnValue;
			}
			return returnValue;
		}

		public static bool IsKillStreak(string coords, bool isBlack, string mode, Bitmap bmp)
		{
			var currentCoords = GetIntCoordsFromSymbolCoords(coords);
			var returnFlag = false;
			var opponentColor = isBlack ? "White" : "Black";
			if (mode != "hit") return false;
			if (GetColorOfCell(new Point(currentCoords.X + 1, currentCoords.Y - 1), bmp) == opponentColor)
			{
				if (GetColorOfCell(new Point(currentCoords.X + 2, currentCoords.Y - 2), bmp) == "BlackCell")
				{
					returnFlag = true;
				}
			}
			if (GetColorOfCell(new Point(currentCoords.X + 1, currentCoords.Y + 1), bmp) == opponentColor)
			{
				if (GetColorOfCell(new Point(currentCoords.X + 2, currentCoords.Y + 2), bmp) == "BlackCell")
				{
					returnFlag = true;
				}
			}
			if (GetColorOfCell(new Point(currentCoords.X - 1, currentCoords.Y - 1), bmp) == opponentColor)
			{
				if (GetColorOfCell(new Point(currentCoords.X - 2, currentCoords.Y - 2), bmp) == "BlackCell")
				{
					returnFlag = true;
				}
			}
			if (GetColorOfCell(new Point(currentCoords.X - 1, currentCoords.Y + 1), bmp) == opponentColor)
			{
				if (GetColorOfCell(new Point(currentCoords.X - 2, currentCoords.Y + 2), bmp) == "BlackCell")
				{
					returnFlag = true;
				}
			}
			return returnFlag;
		}
	}
}