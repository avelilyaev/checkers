using System;
using Android.Content;

namespace CheckersMobile.Tools
{
	public class ActivityBroadcastReceiver : BroadcastReceiver
	{
		public event Action<Context, Intent> Receive;

		public override void OnReceive(Context context, Intent intent)
		{
			if (Receive != null)
				Receive(context, intent);
		}
	}
}