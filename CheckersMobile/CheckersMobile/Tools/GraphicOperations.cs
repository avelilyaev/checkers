using System;
using System.Collections;
using Android.Graphics;

namespace CheckersMobile.Tools
{
	public static class GraphicOperations
	{
		public static Color ConvertIntToRgb(int colorInt)
		{
			return Color.Argb(Color.GetAlphaComponent(colorInt), Color.GetRedComponent(colorInt), Color.GetGreenComponent(colorInt), Color.GetBlueComponent(colorInt));
		}

		public static void DrawBoard(Bitmap image)
		{
			//������������ ����� ������� �����
			var x = 0;
			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < image.Width; j++)
				{
					image.SetPixel(x, j, Color.Black);
					image.SetPixel(j, x, Color.Black);
				}
				x += GameActivity.ScreenWidth / 8 - 1;
			}

			//����������� ��������
			var seedPoint = new Point(GameActivity.ScreenWidth / 16, GameActivity.ScreenWidth / 16);
			//�������� �� ��������
			for (int i = 0; i < 8; i++)
			{
				seedPoint.X = i % 2 == 0 ? (GameActivity.ScreenWidth / 8 - 1 + GameActivity.ScreenWidth / 16) : GameActivity.ScreenWidth / 16 + 1;
				for (int j = 0; j < 4; j++)
				{
					FloodFill(image, seedPoint, BoardManager.BlackCell.ToArgb());
					seedPoint.X += GameActivity.ScreenWidth/4 - 2;
				}
				seedPoint.Y += GameActivity.ScreenWidth/8 - 1;
			}
		}

		public static void FloodFill(Bitmap image, Point node, int replacementColor)
		{
			var width = image.Width;
			var height = image.Height;
			var replacement = replacementColor;
			var target = image.GetPixel(node.X, node.Y);

			if (target == replacement) return;
			var stack = new Stack();
			stack.Push(new Point(node.X, node.Y));
			do
			{
				node = (Point)stack.Pop();
				int x = node.X;
				int y = node.Y;
				while (x > 0 && image.GetPixel(x - 1, y) == target)
				{
					x--;
				}
				var spanUp = false;
				bool spanDown = false;
				while (x < width && image.GetPixel(x, y) == target)
				{
					image.SetPixel(x, y, ConvertIntToRgb(replacement));
					if (!spanUp && y > 0 && image.GetPixel(x, y - 1) == target)
					{
						stack.Push(new Point(x, y - 1));
						spanUp = true;
					}
					else if (spanUp && y > 0
							 && image.GetPixel(x, y - 1) != target)
					{
						spanUp = false;
					}
					if (!spanDown && y < height - 1
						&& image.GetPixel(x, y + 1) == target)
					{
						stack.Push(new Point(x, y + 1));
						spanDown = true;
					}
					else if (spanDown && y < height - 1
							 && image.GetPixel(x, y + 1) != target)
					{
						spanDown = false;
					}
					x++;
				}
			} while (stack.Count > 0);
		}

		public static void Circle(int x1, int y1, int r, Color color, Bitmap image)
		{
			var x = 0;
			var y = r;
			var delta = 1 - 2 * r;
			while (y >= 0)
			{
				image.SetPixel(x1 + x, y1 + y, color);
				image.SetPixel(x1 + x, y1 - y, color);
				image.SetPixel(x1 - x, y1 + y, color);
				image.SetPixel(x1 - x, y1 - y, color);
				var error = 2 * (delta + y) - 1;
				if ((delta < 0) && (error <= 0))
				{
					delta += 2 * ++x + 1;
					continue;
				}
				error = 2 * (delta - x) - 1;
				if ((delta > 0) && (error > 0))
				{
					delta += 1 - 2 * --y;
					continue;
				}
				x++;
				delta += 2 * (x - y);
				y--;
			}
		}

		public static void Line(int x1, int y1, int x2, int y2, Color clr, Bitmap image)
		{
			var steep = (Math.Abs(y2 - y1) > Math.Abs(x2 - x1));
			if (steep)
			{
				var tmp = x1; x1 = y1; y1 = tmp;
				tmp = x2; x2 = y2; y2 = tmp;
			}

			if (x1 > x2)
			{
				var tmp = x1; x1 = x2; x2 = tmp;
				tmp = y1; y1 = y2; y2 = tmp;
			}

			float dx = Math.Abs(x2 - x1);
			float dy = Math.Abs(y2 - y1);

			var error = dx / 2.0f;
			var ystep = (y1 < y2) ? 1 : -1;
			var y = y1;

			var maxX = x2;

			for (int x = x1; x <= maxX; x++)
			{
				if (steep)
				{
					image.SetPixel(y, x, clr);
				}
				else
				{
					image.SetPixel(x, y, clr);
				}

				error -= dy;
				if (!(error < 0)) continue;
				y += ystep;
				error += dx;
			}
		}
	}
}