using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Graphics.Drawables;
using System.Threading;
using CheckersMobile.Tools;
using Android.Content.Res;
using Android.Media;

namespace CheckersMobile
{
	[Activity(Label = "GameActivity")]
	public class GameActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Game);

			var surrenderButton = FindViewById<Button>(Resource.Id.Surrender);
			surrenderButton.Click += surrenderButton_Click;

			ScreenWidth = Resources.DisplayMetrics.WidthPixels;

			IsMyTurn = MyNetworkClass.GetIsServer();

			_imageView = FindViewById<ImageView>(Resource.Id.canvas);

			_textView = FindViewById<TextView>(Resource.Id.text);

			_textViewCount1 = FindViewById<TextView>(Resource.Id.count1);

			_textViewCount2 = FindViewById<TextView>(Resource.Id.count2);

			_image = Bitmap.CreateBitmap(ScreenWidth - 7, ScreenWidth - 7, Bitmap.Config.Argb8888);
			_image.EraseColor(Color.White);

			//������ ����� � ����������� �����
			BoardManager.DrawBoard(_image, _imageView);

			// ������� ��������� � ImageView ��� ���������
			_imageView.SetImageBitmap(_image);

			RefreshScore(!MyNetworkClass.GetIsServer(), CountOfKillsWhite, CountOfKillsBlack);

			//�������, ��� ���
			RefreshTitle();

			_timer1 = new System.Timers.Timer(100) { Enabled = true };
			_timer1.Elapsed += timer1_Tick;
		}

		/// <summary>
		/// ���������� ������� �� ������ "��������������"
		/// </summary>
		private void surrenderButton_Click(object sender, EventArgs e)
		{
			new AlertDialog.Builder(this)
			.SetTitle("���� ��������!")
			.SetMessage(MyNetworkClass.MyName + ", �� ���������!")
			.SetPositiveButton("� ����", (senderAlert, args) => { Finish(); })
			.SetNegativeButton("�����", (senderAlert, args) => { System.Environment.Exit(0); })
			.SetIcon(Resource.Drawable.Icon).Show();
		}

		public override bool OnTouchEvent(MotionEvent e)
		{
			if (e.Action == MotionEventActions.Down)
			{
				var x = Convert.ToInt32(e.GetX());
				var y = Convert.ToInt32(e.GetY() - _textView.Height - 20);
				//--------------------------------------------------------------------------------
				if (y <= ScreenWidth - 7 && y > 0)
				{
					if (IsMyTurn)
					{
						var coords = BoardManager.GetSymbolCoordsFromMouseCoords(new Point(x, y));

						//������ ����� ����----------------------------------------1
						if (!_isClicked)
						{
							var colorOfCell = BoardManager.GetColorOfCell(coords, _image);

							bool isCorrectCondition = (colorOfCell == "Black" || colorOfCell == "BlackQueen") && !MyNetworkClass.GetIsServer()
												   || (colorOfCell == "White" || colorOfCell == "WhiteQueen") && MyNetworkClass.GetIsServer();
							if (isCorrectCondition)
							{
								_isClicked = true;
								FirstCoords = coords;
								BoardManager.HighlightCell(FirstCoords, true, _image, _imageView);
								Message += coords;
							}
						}
						//������ ����� ����--------------------------------------2
						else
						{
							//���� ������ �� ��� �� �����, �� �������� �� �����
							if (coords == FirstCoords)
							{
								BoardManager.HighlightCell(FirstCoords, false, _image, _imageView);
								_isClicked = false;
								Message = null;
							}
							else
								if (BoardManager.IsValidCell(coords, _image))
								{
									//������ ������ ������
									var isBlackColor = !MyNetworkClass.GetIsServer();
									//true - ������ � ���������� �������
									//false - ������ � ���������� ������

									Mode = BoardManager.IsValidDistance(coords, isBlackColor, FirstCoords, _image);
									if (Mode != null)
									{
										Message += coords;
										_isClicked = false;
										BoardManager.HighlightCell(FirstCoords, false, _image, _imageView);
										HomeActivity.MediaPlayer.Start();
										BoardManager.RemoveShape(FirstCoords, _image, _imageView);

										if (coords[1] == '8')
										{
											BoardManager.DrawShapeQueen(coords, isBlackColor ? BoardManager.BlackShape : BoardManager.WhiteShape, _image, _imageView);
										}
										else
										{
											if (Mode == "move" || Mode == "hit")
											{
												BoardManager.DrawShape(coords, isBlackColor ? BoardManager.BlackShape : BoardManager.WhiteShape, _image, _imageView);
											}
											else if (Mode == "MoveQueen" || Mode == "HitQueen")
											{
												BoardManager.DrawShapeQueen(coords, isBlackColor ? BoardManager.BlackShape : BoardManager.WhiteShape, _image, _imageView);
											}

										}
										if (Mode == "hit" || Mode == "HitQueen")
										{
											BoardManager.RemoveShape(BoardManager.KilledShapeCoords, _image, _imageView);
											if (isBlackColor)
											{
												CountOfKillsWhite++;
											}
											else
											{
												CountOfKillsBlack++;
											}
											RefreshScore(isBlackColor, CountOfKillsWhite, CountOfKillsBlack);
										}
										//����, ���������� �� ������ ����� ������
										//���������, ���� �� ��� ����������� ������
										var killStreak = BoardManager.IsKillStreak(coords, isBlackColor, Mode, _image);
										Message = killStreak ? Message + "#" : Message;
										//��������, ��������� ������ � �������� ���� � �������� ���������� ������� ������
										MyNetworkClass.SendMessage(Message);
										IsMyTurn = killStreak ? IsMyTurn : !IsMyTurn;
										RefreshTitle();
										if (killStreak)
										{
											FirstCoords = coords;
											_isClicked = true;
											Message = coords;
											_textView.Text = "������ ����� ������";
										}
										if (!isBlackColor && CountOfKillsBlack == 12 || isBlackColor && CountOfKillsWhite == 12)
										{
											new AlertDialog.Builder(this)
											.SetTitle("���� ��������!")
											.SetMessage(MyNetworkClass.MyName + ", �����������, �� ��������!")
											.SetPositiveButton("� ����", (senderAlert, args) => { Finish(); })
											.SetNegativeButton("�����", (senderAlert, args) => { System.Environment.Exit(0); })
											.SetIcon(Resource.Drawable.Icon).Show();
										}
									}
								}
						}
					}
				}
				_timer1.Start();
			}
			return base.OnTouchEvent(e);
		}

		private void RefreshTitle()
		{
			_textView.Text = IsMyTurn ? "��� ���, " + MyNetworkClass.MyName
				: MyNetworkClass.OpponentName + " �����, ���������...";
		}

		public void RefreshScore(bool isBlackColor, int countOfKillsWhite, int countOfKillsBlack)
		{
			_textViewCount1.Text = MyNetworkClass.MyName + " : " + Convert.ToString(isBlackColor ? countOfKillsWhite : countOfKillsBlack);
			_textViewCount2.Text = MyNetworkClass.OpponentName + " : " + Convert.ToString(isBlackColor ? countOfKillsBlack : countOfKillsWhite);
		}

		/// <summary>
		/// ���������� ��������� ����� 
		/// </summary>
		public static string FirstCoords;

		/// <summary>
		/// ��������� ���/�� ���������� � ������� � ����
		/// </summary>
		public static string Message;

		public static string Mode;

		public static int ScreenWidth;

		/// <summary>
		/// ���� ��� ����������� ����������� �����
		/// </summary>
		public static bool IsMyTurn;

		/// <summary>
		/// ������� ����� �����
		/// </summary>
		public static int CountOfKillsWhite;

		/// <summary>
		/// ������� ����� ������
		/// </summary>
		public static int CountOfKillsBlack;

		public static void ResetScore()
		{
			CountOfKillsWhite = 0;
			CountOfKillsBlack = 0;
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			RunOnUiThread(() =>
			{
				if (_myThread == null)
				{
					_myThread = new Thread(_getMessege);
					_myThread.Start();
				}
				if (Message != string.Empty && _myThread.ThreadState == ThreadState.Stopped && Message != "exit" && !IsMyTurn && Message != null)
				{
					HomeActivity.MediaPlayer.Start();
					var fstCoords = ConvertCoords(Message.Substring(0, 2));
					var coords = ConvertCoords(Message.Substring(2, 2));

					var mode = BoardManager.IsValidDistance(coords, MyNetworkClass.GetIsServer(), fstCoords, _image);
					if (mode == "hit" || mode == "HitQueen")
					{
						BoardManager.RemoveShape(BoardManager.KilledShapeCoords, _image, _imageView);
						var isBlackColor = !MyNetworkClass.GetIsServer();
						if (isBlackColor)
						{
							CountOfKillsBlack++;
						}
						else
						{
							CountOfKillsWhite++;
						}
						RefreshScore(isBlackColor, CountOfKillsWhite, CountOfKillsBlack);
					}

					var colorOfCell = BoardManager.GetColorOfCell(fstCoords, _image);
					BoardManager.RemoveShape(fstCoords, _image, _imageView);
					var isQueen = colorOfCell == "WhiteQueen" || colorOfCell == "BlackQueen";

					if (coords[1] == '1')
					{
						BoardManager.DrawShapeQueen(coords, MyNetworkClass.GetIsServer() ? BoardManager.BlackShape : BoardManager.WhiteShape, _image, _imageView);
					}
					else
					{
						if (isQueen)
						{
							BoardManager.DrawShapeQueen(coords, MyNetworkClass.GetIsServer() ? BoardManager.BlackShape : BoardManager.WhiteShape, _image, _imageView);
						}
						else
						{
							BoardManager.DrawShape(coords, MyNetworkClass.GetIsServer() ? BoardManager.BlackShape : BoardManager.WhiteShape, _image, _imageView);
						}
					}
					//���� ��������� � ����� ������������� �� ������ "#", �� ��� ������, ��� ��������� ����� ��������� ����� ������
					IsMyTurn = Message[Message.Length - 1] == '#' ? IsMyTurn : !IsMyTurn;
					RefreshTitle();
					if (!MyNetworkClass.GetIsServer() && CountOfKillsBlack == 12 || MyNetworkClass.GetIsServer() && CountOfKillsWhite == 12)
					{
						new AlertDialog.Builder(this)
						.SetTitle("���� ��������!")
						.SetMessage(MyNetworkClass.MyName + ", �� ���������!")
						.SetPositiveButton("� ����", (senderAlert, args) => { Finish(); })
						.SetNegativeButton("�����", (senderAlert, args) => { System.Environment.Exit(0); })
						.SetIcon(Resource.Drawable.Icon).Show();
						IsMyTurn = false;
					}
					Message = null;
					_myThread = null;

					_imageView.SetImageBitmap(_image);
				}
				else if (Message == "exit")
				{
					new AlertDialog.Builder(this)
					.SetTitle("��������� ����� �������� ����!")
					.SetMessage(MyNetworkClass.MyName + ", �� ��������!")
					.SetPositiveButton("� ����", (senderAlert, args) => { Finish(); })
					.SetNegativeButton("�����", (senderAlert, args) => { System.Environment.Exit(0); })
					.SetIcon(Resource.Drawable.Icon).Show();
					Message = null;
					IsMyTurn = false;
				}
			});
		}

		private static string ConvertCoords(string coords)
		{
			var returnCoords = string.Empty;
			returnCoords += Convert.ToChar(8 - (coords[0] - 65) + 64);
			returnCoords += Convert.ToString(8 - (Convert.ToInt32(coords[1]) - 48) + 1);

			return returnCoords;
		}

		private static void _getMessege()
		{
			Message = MyNetworkClass.GetMessage();
		}

		/// <summary>
		/// Is clicked on shape
		/// </summary>
		private bool _isClicked;

		/// <summary>
		/// ������
		/// </summary>
		private Bitmap _image;

		private Thread _myThread;

		private ImageView _imageView;

		private TextView _textView;

		private TextView _textViewCount1;

		private TextView _textViewCount2;

		private System.Timers.Timer _timer1;
	}
}