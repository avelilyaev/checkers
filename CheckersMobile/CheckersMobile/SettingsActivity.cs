using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CheckersMobile.Tools;

namespace CheckersMobile
{
	[Activity(Label = "SettingsActivity")]
	public class SettingsActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Settings);
			
			_toggleSoundButton = FindViewById<Button>(Resource.Id.toggleSound);
			_toggleSoundButton.Click += toggleSoundButton_Click;
			_toggleSoundButton.Text = HomeActivity.IsSoundTurnedOn ? "��������� ����" : "�������� ����";

			var backButton = FindViewById<Button>(Resource.Id.back);
			backButton.Click += backButton_Click;
		}

		/// <summary>
		/// ���������� ������� �� ������ ���������/���������� �����
		/// </summary>
		private void toggleSoundButton_Click(object sender, EventArgs e)
		{
			_toggleSoundButton.Text = HomeActivity.IsSoundTurnedOn ? "�������� ����" : "��������� ����";
			var backgroundVolume = HomeActivity.IsSoundTurnedOn ? 0f : 0.1f;
			var volume = HomeActivity.IsSoundTurnedOn ? 0f : 1f;
			HomeActivity.BackgroundMediaPlayer.SetVolume(backgroundVolume, backgroundVolume);
			HomeActivity.MediaPlayer.SetVolume(volume, volume);
			HomeActivity.IsSoundTurnedOn = !HomeActivity.IsSoundTurnedOn;
		}

		/// <summary>
		/// ���������� ������� �� ������ "�����"
		/// </summary>
		private void backButton_Click(object sender, EventArgs e)
		{
			Finish();
		}

		/// <summary>
		/// ������ ���������/���������� �����
		/// </summary>
		private Button _toggleSoundButton;
	}
}