using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CheckersMobile.Tools;

namespace CheckersMobile
{
	[Activity(Label = "������� �����", MainLauncher = true, Icon = "@drawable/icon")]
	public class HomeActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Home);
			
			BackgroundMediaPlayer = MediaPlayer.Create(this, Resource.Raw.whereIsMyMind);
			BackgroundMediaPlayer.Looping = true;
			BackgroundMediaPlayer.SetVolume(0.1f, 0.1f);
			BackgroundMediaPlayer.Start();
			
			MediaPlayer = MediaPlayer.Create(this, Resource.Raw.click);
			
			IsSoundTurnedOn = true;
			
			var chooseBluetoothButton = FindViewById<Button>(Resource.Id.Bluetooth);
			chooseBluetoothButton.Click += chooseBluetoothButton_Click;

			var chooseWiFiButton = FindViewById<Button>(Resource.Id.WiFi);
			chooseWiFiButton.Click += chooseWiFiButton_Click;

			var settingsButton = FindViewById<Button>(Resource.Id.SettingsButton);
			settingsButton.Click += settingsButton_Click;

			var exitButton = FindViewById<Button>(Resource.Id.exitButton);
			exitButton.Click += exitButton_Click;
		}

		/// <summary>
		/// ���������� ������� �� ������ "Bluetooth"
		/// </summary>
		private void chooseBluetoothButton_Click(object sender, EventArgs e)
		{
			if (BluetoothAdapter.DefaultAdapter != null)
			{
				StartActivity(typeof(RoleSelectionActivity));
			}
			else
			{
				Toast.MakeText(this, "���� ���������� �� �������� Bluetooth ���������", ToastLength.Long).Show();
			}
		}

		/// <summary>
		/// ���������� ������� �� ������ "Wi-Fi"
		/// </summary>
		private void chooseWiFiButton_Click(object sender, EventArgs e)
		{
			StartActivity(typeof(WifiSettingsActivity));
		}

		/// <summary>
		/// ���������� ������� �� ������ "���������"
		/// </summary>
		private void settingsButton_Click(object sender, EventArgs e)
		{
			StartActivity(typeof(SettingsActivity));
		}

		/// <summary>
		/// ���������� ������� �� ������ "�����"
		/// </summary>
		private void exitButton_Click(object sender, EventArgs e)
		{
			System.Environment.Exit(0);
		}

		/// <summary>
		/// ������, ����������� ��� ������������ ������� ������
		/// </summary>
		public static MediaPlayer BackgroundMediaPlayer;

		/// <summary>
		/// ������, ����������� ��� ��������������� �����
		/// </summary>
		public static MediaPlayer MediaPlayer;

		/// <summary>
		/// ����, ������������ �����
		/// </summary>
		public static bool IsSoundTurnedOn;
	}
}