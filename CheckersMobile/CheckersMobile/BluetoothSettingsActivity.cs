using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Bluetooth;
using Android.Media;
using CheckersMobile.Tools;
using Java.IO;
using Java.Util;

namespace CheckersMobile
{
	[Activity(Label = "BluetoothSettingsActivity")]
	public class BluetoothSettingsActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.BluetoothSettings);

			_bondedlistView = FindViewById<ListView>(Resource.Id.bondedlistView);
			_bondedlistView.ItemClick += bondedlistView_ItemClick;

			_foundedlistView = FindViewById<ListView>(Resource.Id.foundedlistView);
			_foundedlistView.ItemClick += foundedlistView_ItemClick;

			MyNetworkClass.FoundDevices = new List<BluetoothDevice>();

			_bondedlistView.Adapter = new ArrayAdapter<string>(this,
									Android.Resource.Layout.SimpleListItem1,
									BluetoothAdapter.DefaultAdapter.BondedDevices.Select(x => x.Name).ToArray());	

			#region BroadcastReceiverRegistration
			_broadcastReceiver = new ActivityBroadcastReceiver();
			_broadcastReceiver.Receive += (Context context, Intent intent) =>
			{
				if (BluetoothDevice.ActionFound.Equals(intent.Action))
				{
					var device = (BluetoothDevice)intent.GetParcelableExtra(BluetoothDevice.ExtraDevice);
					if (device.BondState != Bond.Bonded)
					{
						if (!MyNetworkClass.FoundDevices.Any(item => item.Address == device.Address))
						{
							MyNetworkClass.FoundDevices.Add(device);
							Toast.MakeText(this, "������� ����������: " + device.Name, ToastLength.Long).Show();
						}
						_foundedlistView.Adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, MyNetworkClass.FoundDevices.Select(x => x.Name).ToArray());
					}
				}
			};
			RegisterReceiver(_broadcastReceiver, new IntentFilter(BluetoothDevice.ActionFound));
			#endregion

			var stopFindDevicesButton = FindViewById<Button>(Resource.Id.StopFindDevicesButton);
			stopFindDevicesButton.Click += stopFindDevicesButton_Click;

			var findDevicesButton = FindViewById<Button>(Resource.Id.FindDevicesButton);
			findDevicesButton.Click += findDevicesButton_Click;

			var backButton = FindViewById<Button>(Resource.Id.backFromBluetoothSettingsButton);
			backButton.Click += backButton_Click;
		}

		/// <summary>
		/// ���������� ������� �� ������ "���������� �����"
		/// </summary>
		private void stopFindDevicesButton_Click(object sender, EventArgs e)
		{
			BluetoothAdapter.DefaultAdapter.CancelDiscovery();
		}

		/// <summary>
		/// ���������� ������� �� ������ "����� ��������� Bluetooth ���������"
		/// </summary>
		private void findDevicesButton_Click(object sender, EventArgs e)
		{
			BluetoothAdapter.DefaultAdapter.StartDiscovery();
		}

		/// <summary>
		/// ���������� ������� �� ������ "�����"
		/// </summary>
		private void backButton_Click(object sender, EventArgs e)
		{
			Finish();
		}

		/// <summary>
		/// ���������� ����� �� ������� ������ ��������� ���������
		/// </summary>
		private void bondedlistView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			Toast.MakeText(this, BluetoothAdapter.DefaultAdapter.BondedDevices.ToArray()[e.Position].Address + " was selected!", ToastLength.Short).Show();
			MyNetworkClass.MacAddress = BluetoothAdapter.DefaultAdapter.BondedDevices.ToArray()[e.Position].Address;
			MyNetworkClass.OpponentName = BluetoothAdapter.DefaultAdapter.BondedDevices.ToArray()[e.Position].Name;
			MyNetworkClass.InitializeForSlave();
			StartActivity(typeof(GameActivity));
		}

		/// <summary>
		/// ���������� ����� �� ������� ��������� ���������
		/// </summary>
		private void foundedlistView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
		{
			Toast.MakeText(this, MyNetworkClass.FoundDevices[e.Position].Address + " was selected!", ToastLength.Short).Show();
			MyNetworkClass.MacAddress = MyNetworkClass.FoundDevices[e.Position].Address;
			MyNetworkClass.OpponentName = MyNetworkClass.FoundDevices[e.Position].Name;
			MyNetworkClass.InitializeForSlave();
			StartActivity(typeof(GameActivity));
		}

		/// <summary>
		/// ���������� ��������� � ���� ������ ���������� Bluetooth ����������
		/// </summary>
		private ActivityBroadcastReceiver _broadcastReceiver;

		/// <summary>
		/// ������ ����������� ����� Bluetooth ���������
		/// </summary>
		private ListView _bondedlistView;

		/// <summary>
		/// ������ ��������� Bluetooth ���������
		/// </summary>
		private ListView _foundedlistView;
	}
}