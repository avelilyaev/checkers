using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Bluetooth;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CheckersMobile.Tools;

namespace CheckersMobile
{
	[Activity(Label = "RoleSelectionActivity")]
	public class RoleSelectionActivity : Activity
	{
		protected override void OnCreate(Bundle bundle)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.RoleSelection);

			var createGameButton = FindViewById<Button>(Resource.Id.CreateGame);
			createGameButton.Click += CreateGameButton_Click;

			var connectButton = FindViewById<Button>(Resource.Id.Connect);
			connectButton.Click += ConnectButton_Click;

			var backButton = FindViewById<Button>(Resource.Id.backFromRoleSelectionButton);
			backButton.Click += backButton_Click;

			if (!BluetoothAdapter.DefaultAdapter.IsEnabled)
			{
				StartActivityForResult(new Intent(BluetoothAdapter.ActionRequestEnable), 10);
				Finish();
			}
		}

		/// <summary>
		/// ���������� ������� �� ������ "������� ����"
		/// </summary>
		private void CreateGameButton_Click(object sender, EventArgs e)
		{
			if (BluetoothAdapter.DefaultAdapter.IsEnabled)
			{
				//������ ����������-������ �������
				StartActivity(new Intent(BluetoothAdapter.ActionRequestDiscoverable).PutExtra(BluetoothAdapter.ExtraDiscoverableDuration, 300));

				MyNetworkClass.InitializeForMaster();
				StartActivity(typeof(GameActivity));
			}
		}

		/// <summary>
		/// ���������� ������� �� ������ "������������"
		/// </summary>
		private void ConnectButton_Click(object sender, EventArgs e)
		{
			if (BluetoothAdapter.DefaultAdapter.IsEnabled)
			{
				StartActivity(typeof(BluetoothSettingsActivity));
			}
		}

		/// <summary>
		/// ���������� ������� �� ������ "�����"
		/// </summary>
		private void backButton_Click(object sender, EventArgs e)
		{
			Finish();
		}
	}
}